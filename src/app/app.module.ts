import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material.module';
import { Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DetailComponent } from './components/detail/detail.component';
import { ListComponent } from './components/list/list.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { EditCreateComponent } from './components/edit-create/edit-create.component';
import { OwlModule } from 'ngx-owl-carousel';
import { SliderComponent } from './components/slider/slider.component';
import { NewPostComponent } from './components/new-post/new-post.component';
import { CategoryComponent } from './components/category/category.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { FooterComponent } from './components/footer/footer.component';

// export function WpApiLoaderFactory(http: Http) {
// 	return new WpApiStaticLoader(http, 'https://unitymta.com/wp-json/wp/v2/', '');
// }
@NgModule({
	declarations: [
		AppComponent,
		DetailComponent,
		ListComponent,
		AuthenticationComponent,
		EditCreateComponent,
		SliderComponent,
		NewPostComponent,
		CategoryComponent,
		CategoryListComponent,
		FooterComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		AppMaterialModule,
		HttpClientModule,
		FormsModule,
		OwlModule
		// WpApiModule.forRoot({
		// 	provide: WpApiLoader,
		// 	useFactory: (WpApiLoaderFactory),
		// 	deps: [Http]
		// })
	],
	providers: [],
	bootstrap: [AppComponent]
})

export class AppModule { }
