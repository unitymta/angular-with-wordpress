import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})

export class WordpressService {
	apiUrl = 'https://unitymta.com/wp-json/wp/v2/';
	token = JSON.parse(localStorage.getItem('tokenLocal'));
	headers: HttpHeaders = new HttpHeaders({
		'Authorization': 'Bearer ' + this.token,
		'Content-Type': 'application/json'
	});
	constructor(private http: HttpClient) { }

	getPost(): Observable<any[]> {
		return this.http.get<any[]>(`${this.apiUrl}posts`, {
			params: {
				per_page: '100'
			}
		});
	}

	getPostId(id: any): Observable<any> {
		return this.http.get<any>(`${this.apiUrl}posts/${id}`);
	}

	getImageFeature(id: any): Observable<any> {
		return this.http.get<any>(`${this.apiUrl}media/${id}`);
	}

	getCategoryList(id: any): Observable<any> {
		return this.http.get<any>(`${this.apiUrl}posts?categories=${id}`);
	}

	createPost(new_post: any): Observable<any> {
		return this.http.post<any>(`${this.apiUrl}posts`, new_post, { headers: this.headers });
	}

	updatePost(id: any, data_put: any): Observable<any> {
		return this.http.put<any>(`${this.apiUrl}posts/${id}`, data_put, { headers: this.headers });
	}

	deletePost(id: any): Observable<any> {
		console.log('dsddsadasd')
		return this.http.delete<any>(`${this.apiUrl}posts/${id}`, { headers: this.headers });
	}
}
