import { NgModule } from '@angular/core';
import { MatToolbarModule, MatCardModule, MatButtonModule, MatInputModule, MatFormFieldModule } from '@angular/material';

const materialModules = [
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
];

@NgModule({
    imports: materialModules,
    exports: materialModules
})
export class AppMaterialModule { }