import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(private router: Router) {

	}
	canActivate(): boolean {
		return this.verifyLogin();
	}

	verifyLogin(): boolean {
		if (!this.isLoggedIn()) {
			this.router.navigate(['']);
			return false;
		}
		else {
			if (this.isLoggedIn()) {
				return true;
			}
		}
	}

	public isLoggedIn(): boolean {
		let status = false;
		if (localStorage.getItem('tokenLocal')) {
			status = true;
		}
		else {
			status = false;
		}
		return status;
	}
}
