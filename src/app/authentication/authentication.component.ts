import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
	selector: 'app-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
	apiHost = 'https://unitymta.com/wp-json';
	account = JSON.parse(localStorage.getItem('userName'));
	countLoginFail = 0;
	disabled = false;

	@Input() token;
	// @Output() tokenChange = new EventEmitter<string>();

	constructor(private http: HttpClient,
		private router: Router) { }

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.checkLoginFail();
	}

	private checkLoginFail() {
		let userLogin = document.getElementById('login');
		let userPassword = document.getElementById('password');
		let submit = document.getElementById('submit');
		
		var myTimer = setInterval(() => {					
			let l = userLogin.getAttribute('disabled');			
			let p = userPassword.getAttribute('disabled');
			let s = submit.getAttribute('disabled');
			if (l === 'disabled' || p === 'disabled' || s === 'disabled') {
				this.countLoginFail = 0;
				userLogin['value'] = '';
				userPassword['value'] = ''
				setTimeout(() => {
					userLogin.removeAttribute('disabled');
					userPassword.removeAttribute('disabled');
					submit.removeAttribute('disabled');
				}, 300000);
				confirm('WARNING: You enter too much wrong. Please come back after 5 minutes')
				clearInterval(myTimer);
			}
		}, 500)
	}

	auth() {
		let userLogin = document.getElementById('login');
		let userPassword = document.getElementById('password');
		let submit = document.getElementById('submit');
		if (this.countLoginFail >= 2) {
			this.disabled = true;
			userLogin.setAttribute('disabled', 'disabled');
			userPassword.setAttribute('disabled', 'disabled');
			submit.setAttribute('disabled', 'disabled');
		}

		if (userLogin['value'] !== '' && userPassword['value'] !== '') {
			this.http.post(this.apiHost + '/jwt-auth/v1/token', {
				username: userLogin['value'],
				password: userPassword['value']
			}).subscribe(
				(data) => {
					this.account = data['user_display_name'];
					if (data['token']) {
						this.token = data['token'];
						localStorage.setItem('tokenLocal', JSON.stringify(data['token']));
						localStorage.setItem('userName', JSON.stringify(this.account));
						// this.tokenChange.emit(this.token);
						window.location.reload();
					}
				},
				error => {
					this.countLoginFail++;
					alert('Incorrect username or password. Please try again.');
				}
			);
		} else {
			this.countLoginFail++;
			alert('Please enter username and password.');
		}
	}

	logOut() {
		localStorage.removeItem('tokenLocal');
		localStorage.removeItem('userName');
		window.location.reload();
	}
}
