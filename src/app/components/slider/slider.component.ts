import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
	mySlideOptions = { items: 3, dots: false, nav: true };
	
	constructor() { }

	ngOnInit() {
	}

}
