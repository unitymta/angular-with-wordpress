import { Component, OnInit, Input } from '@angular/core';
import { WordpressService } from '../../wordpress.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-edit-create',
	templateUrl: './edit-create.component.html',
	styleUrls: ['./edit-create.component.scss']
})
export class EditCreateComponent implements OnInit {
	@Input() token;
	tokenLocal = localStorage.getItem('tokenLocal');
	status = '';
	new_post = {
		title: '',
		content: '',
		status: 'publish'
	}
	edit_post = {
		title: '',
		content: '',
		status: 'publish'
	}

	constructor( 
		private wp: WordpressService, 
		private location: Location, 
		private route: ActivatedRoute ) { }

	ngOnInit() {
		let temp = this.route.queryParams['value']['posts'];		
		if( this.tokenLocal != null) {
			if( temp === 'create') {
				this.status = 'create';
			}
			if( temp === 'edit'){
				this.status = 'edit';
			}

		}

		let idUrl = this.route.queryParams['value']['id'];
		// this.wp.getPostId().subscribe()
		console.log(idUrl,'URL')
		this.wp.getPostId(idUrl).subscribe(data => { 
			this.edit_post['title'] = data['title']['rendered'];
			this.edit_post['content'] = data['content']['rendered']; 
		});
	}

	createPost() {	
		alert('Created post success!');
		return this.wp.createPost(this.new_post).subscribe();
	}

	editPost() {
		alert('Update post success!');
		let idUrl = this.route.queryParams['value']['id'];
		this.wp.updatePost(idUrl, this.edit_post).subscribe();
	}
}
