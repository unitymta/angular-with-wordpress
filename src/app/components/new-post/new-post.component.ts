import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { WordpressService } from '../../wordpress.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-new-post',
	templateUrl: './new-post.component.html',
	styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
	posts: any;
	featured_media: any = [];	

	constructor(private wp: WordpressService) {
		this.getNewPost();
	}

	ngOnInit() {
	}

	getNewPost(): any {
		this.wp.getPost().pipe(
			map(data => data.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()))
		)
			.subscribe(value => {
				this.posts = value;
				value.map((vl) => {
					if( vl['featured_media'] != 0){
						this.wp.getImageFeature(vl['featured_media']).subscribe(img => {
							this.featured_media.push(img);
						});
					}					
				});
			});
	}
}
