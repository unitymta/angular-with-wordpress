import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { WordpressService } from '../../wordpress.service';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

	posts: Observable<any[]>;	

	constructor(private wp: WordpressService) {
		this.posts = this.wp.getPost();
	}

	ngOnInit() {
	}
	
}
