import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../../wordpress.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: 'app-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

	post: any;
	token = localStorage.getItem('tokenLocal');

	constructor(private wp: WordpressService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location) { }

	ngOnInit() {
		this.getPost();
	}

	getPost(): void{
		let temp = this.route.snapshot.paramMap.get('id');
		this.wp.getPost().subscribe(posts => {
			posts.map(value => {
				let t: any = value["id"];		
				if(t.toString() === temp){
					this.post = value;
				}
			});
		});
	}

	goBack(): void {
		this.location.back();
	}

	deletePost(): void{
		if(this.token != null){
			let temp = this.route.snapshot.paramMap.get('id');
			this.wp.deletePost(temp).subscribe();
			this.router.navigate(['/']);
		} else {
			alert('Please login then delete post!')
		}		
	}

}
