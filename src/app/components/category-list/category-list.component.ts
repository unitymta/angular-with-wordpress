import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../../wordpress.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
	selector: 'app-category-list',
	templateUrl: './category-list.component.html',
	styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
	categoryList: any;
	featuredMedia = [];
	showEdit = false;
	tokenLocal = localStorage.getItem('tokenLocal');

	constructor(private wp: WordpressService,
			private router: ActivatedRoute,
			private route: Router ) { }

	ngOnInit() {
		this.getCategory();		
	}

	getCategory(): void {
		let t = this.router.snapshot.paramMap.get('id');
		this.wp.getCategoryList(t).subscribe(data => {			
			this.categoryList = data;			
			data.map(vl => {
				if( vl['featured_media'] != 0){
					this.wp.getImageFeature(vl['featured_media']).subscribe(img => {
						this.featuredMedia.push(img);
					});
				}				
			})
		});
	}

	createPost(str: any): void {		
		if( localStorage.getItem('tokenLocal') == null ){
			alert('Please login then create new post -> Come on baby');
		}
		else {
			this.route.navigate(['/edit-create'], { queryParams: {posts: str} });
		}
	}

	editPost(str: any, id: any): void {		
		if( localStorage.getItem('tokenLocal') == null ){
			alert('Please login then edit post -> Come on baby');
		}
		else {
			console.log(this.categoryList,'categoryList')
			this.route.navigate(['/edit-create'], { queryParams: {posts: str, id: id} });
		}
	}
	
}
