import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { EditCreateComponent } from './components/edit-create/edit-create.component';
import { AuthGuard } from './authentication/auth.guard';
import { CategoryListComponent } from './components/category-list/category-list.component';

const routes: Routes = [
	{ path: '', component: ListComponent },
	{ path: 'detail', component: ListComponent },
	{ path: 'detail/:id', component: DetailComponent },
	{ path: 'edit-create', component: EditCreateComponent, canActivate: [AuthGuard] },
	{ path: 'category/:id', component: CategoryListComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
