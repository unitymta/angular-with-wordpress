import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	
	title = 'angular-wordpress-api';
	token = JSON.parse(localStorage.getItem('tokenLocal'));

	constructor() {		
		// localStorage.removeItem('tokenLocal')	;
	}	
}
